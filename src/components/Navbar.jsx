import React from "react";
import "../styles/navstyle.css";
const Navbar = () => {
  return (
    <header className="header">
      <h3 className="logo">Mero Todo List</h3>
    </header>
  );
};

export default Navbar;
