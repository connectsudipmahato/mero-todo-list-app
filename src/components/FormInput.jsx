import React, { useState, useContext } from "react";
import { todoContext } from "./DataProvider";
import { v4 as uuidv4 } from "uuid";

const FormInput = () => {
  const { todos, setTodos } = useContext(todoContext);

  const [todoName, setTodoName] = useState("");

  const handleFormSubmit = (e) => {
    const newTodoData = {
      id: uuidv4(),
      textName: todoName,
      completed: false,
    };
    e.preventDefault();
    setTodos([...todos, newTodoData]);
    setTodoName("");
  };

  return (
    <>
      <form autoComplete="off" onSubmit={handleFormSubmit}>
        <input
          type="text"
          onChange={(e) => setTodoName(e.target.value)}
          value={todoName}
        />
        <button type="submit">Add Todo</button>
      </form>
    </>
  );
};

export default FormInput;
