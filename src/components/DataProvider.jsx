import React, { createContext, useState, useEffect } from "react";
// creating context
export const todoContext = createContext(null);

// to get the data from the local storage

const getLocalItems = () => {
  let resultTodoList = localStorage.getItem("todos");

  if (resultTodoList) {
    // converting String data of todos into Array
    return JSON.parse(localStorage.getItem("todos"));
  } else {
    return [];
  }
};
export const DataProvider = (props) => {
  const [todos, setTodos] = useState(getLocalItems());

  // To add data to the local storage
  // whenever the values of todos will be updated the useEffect will come into action and that updated value will be stored in  the local storage
  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  const deleteTodo = (id) => {
    const updatedTodos = todos.filter((todoName) => todoName.id !== id);
    setTodos(updatedTodos);
  };

  const completeTodo = (id) => {
    let updatedTodos = todos.map((todoName) => {
      if (todoName.id === id) {
        todoName.completed = !todoName.completed;
      }
      return todoName;
    });
    setTodos(updatedTodos);
  };
  return (
    <todoContext.Provider value={{ todos, setTodos, deleteTodo, completeTodo }}>
      {props.children}
    </todoContext.Provider>
  );
};
