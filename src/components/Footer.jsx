import React from "react";
import "../styles/footerstyle.css";
export default function Footer() {
  return (
    <footer className="footer">
      <p>2022 copyright &copy; Mero Todo List</p>
      <p>Sudip Kumar Mahato </p>
    </footer>
  );
}
