import React, { useContext } from "react";
import { todoContext } from "./DataProvider";

const TodoList = () => {
  const { todos, deleteTodo, completeTodo } = useContext(todoContext);

  return (
    <>
      {todos.map((todoName) => (
        // Unique Key of todo
        <div key={todoName.id} className="todo">
          <div className="todo-text">
            <div className="todotext">{todoName.textName}</div>
            <input
              type="checkbox"
              id="completed"
              checked={todoName.completed}
              onChange={() => completeTodo(todoName.id)}
            />
            {/* delete button to delete todo of todos */}
            <button onClick={() => deleteTodo(todoName.id)}>Delete</button>
          </div>
        </div>
      ))}
    </>
  );
};

export default TodoList;
