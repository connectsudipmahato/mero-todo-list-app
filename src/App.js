import React from "react";
import "./styles/Todostyle.css";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer.jsx";
import FormInput from "./components/FormInput";
import TodoList from "./components/TodoList";
import { DataProvider } from "./components/DataProvider";
function App() {
  return (
    <>
      <Navbar />
      <div id="todo-list-container">
        <DataProvider>
          <FormInput />
          <TodoList />
        </DataProvider>
      </div>
      <Footer />
    </>
  );
}

export default App;
